﻿#include "yasuo.h"
#include "mainwindow.h"

int pow_c(int i);
unsigned char change(char data[]);
char *code(unsigned char temp,int num);
Node_T huffman[512], tmp;

void yasuo(char const *infilename, char const *outfilename)
{

    long oldfilelength = 0;
    long newfilelength = 108;
    int leafnum;
    int pointnum;
    int i;
    int j;
    unsigned char temp;
    for(i=0;i<256;i++)
    {
         huffman[i].count=0;
         huffman[i].data=(unsigned char)i;
         huffman[i].parent = 0;
   }
   ifstream infile(infilename,ios::in|ios::binary);
   while(infile.peek()!=EOF)
   {
         infile.read((char *)&temp,sizeof(unsigned char));
         huffman[temp].count++;
         oldfilelength++;
   }
   infile.close();
   for(i=0;i<256-1;i++)
   {
        for(j=0;j<256-1-i;j++)
        {
                if(huffman[j].count<huffman[j+1].count)
                {
                    tmp=huffman[j];
                    huffman[j]=huffman[j+1];
                    huffman[j+1]=tmp;
               }
        }
   }
   for(i=0;i<256;i++)
   {
      if(huffman[i].count==0) break;
   }
   leafnum=i;
   pointnum=2*leafnum-1;
//建立二叉树
   long min;
   int m1,m2;
   for(i=leafnum;i<pointnum;i++)
   {
        min=999999999;
        for(j=0;j<i;j++)
            if(huffman[j].parent==0&&huffman[j].count<min)
            {
                min=huffman[j].count;
                m1=j;
            }
        huffman[m1].parent=i;

        min=999999999;
        for(j = 0; j < i; j++)
           if(huffman[j].parent==0&&huffman[j].count<min)
           {
               min=huffman[j].count;
               m2=j;
           }
        huffman[m2].parent=i;
        huffman[i].count=huffman[m1].count+huffman[m2].count;
        huffman[i].lch=m1;
        huffman[i].rch=m2;
        huffman[i].parent = 0;
   }
//哈夫曼编码
    char tmp[256];
    tmp[255]='\0';
    int start;
    int child;
    int father;
    for(i=0;i<leafnum;i++)
    {
         start=255;
         for(child=i,father=huffman[i].parent;father!=0;child=father,father=huffman[father].parent)
         {
            if(huffman[father].lch==child)
                 tmp[--start]='0';
            else
                 tmp[--start]='1';
         }
         strcpy(huffman[i].code,&tmp[start]);
   }


//对文件进行编码
   infile.open(infilename,ios::in|ios::binary);
   infile.clear();
   infile.seekg(0);
   ofstream outfile(outfilename,ios::out|ios::binary);
   outfile.write((char *) infilename, 100);
   outfile.write((char *)&oldfilelength,sizeof(long));
   char save[513]="\0";
   outfile.seekp(108);
   while(infile.peek()!=EOF)
   {
        infile.read((char *)&temp,sizeof(unsigned char));
        strcat(save,code(temp,leafnum));
        while(strlen(save)>=8)
        {
             temp=change(save);
             outfile.write((char *)&temp,sizeof(unsigned char));
             newfilelength++;
             strcpy(save,save+8);
        }
   }
   if(strlen(save)>0)
   {
         strcat(save,"0000000");
         temp=change(save);
         outfile.write((char *)&temp,sizeof(unsigned char));
         newfilelength++;
   }
   outfile.seekp(104);
   outfile.write((char *)&newfilelength,sizeof(long));
   infile.close();

//将哈夫曼编码写入文件
   long bytelen;
   outfile.clear();
   outfile.seekp(newfilelength);
   outfile.write((char *)&leafnum,sizeof(long));
   for(i=0;i<leafnum;i++)
   {
         outfile.write((char *)&huffman[i].data,sizeof(unsigned char));
         huffman[i].count=strlen(huffman[i].code);
         outfile.write((char *)&huffman[i].count,sizeof(unsigned char));
         if(huffman[i].count%8==0)
            bytelen=huffman[i].count/8;
         else
         {
             bytelen=huffman[i].count/8+1;
             strcat(huffman[i].code,"0000000");

        }
        for(int j=0;j<bytelen;j++)
        {
             temp=change(huffman[i].code);
             outfile.write((char *)&temp,sizeof(unsigned char));
             strcpy(huffman[i].code,huffman[i].code+8);
        }
   }
}

int pow_c(int i)
{
    int a = 1;
    int j;
    for(j = 8-i-1; j > 0; j--)
    {
          a *= 2;
    }
    return a;
}

unsigned char change(char data[])
{
       unsigned char ch = 0;
       for(int i = 0; i < 8; i++)
          if(data[i] != 0)
          {
               ch = ch + (int)(data[i] - '0') * pow_c(i);
          }
       return ch;
}

char *code(unsigned char temp, int num)
{
     for(int i = 0; i < num; i++)
     {
        if(temp == huffman[i].data)
            return huffman[i].code;
    }
    return NULL;
}
