﻿#ifndef HUFFUCODE_H
#define HUFFUCODE_H
#include <QString>
#include <QFile>
#include <cstring>
#include <QMessageBox>
#include <fstream>
#include <iomanip>
using namespace std;

void yasuo(const char *, const char *);

typedef struct node
{
    unsigned char data;          //结点信息
    long count;               //结点权重
    int parent, lch, rch;
    char code[256];
} Node_T;

#endif // HUFFUCODE_H
