﻿#include "jieya.h"
#include "mainwindow.h"
#include "yasuo.h"
#include <iostream>

extern Node_T huffman[512],tmp;
void change(unsigned char a,char code[]);
int pipei(char buf[],Node_T head[],int n,unsigned char &c);
void fuzhi(char buf[],char a[],int j);

void jieya(char const *infilename, char const *outfilename)
{
     long oldfilelength;
     long newfilelength;
     char filename[100];
     memset(filename, '\0', sizeof(filename));
     int leafnum;
     int i;
     char code[9];
     unsigned char temp;
     long hasread=0;
     long haswrite=0;
     long codelength;
//读入待解压文件
    void change(unsigned char a,char code[]);
    ifstream infile(infilename,ios::binary | ios::in);
    if(!infile)
    {
        return;
   }
   infile.read((char *)&filename, 100);
   infile.read((char *)&oldfilelength, sizeof(long));
   infile.read((char *)&newfilelength, sizeof(long));
   infile.seekg(newfilelength);
   infile.read((char *)&leafnum, sizeof(int));
//哈夫曼编码
   infile.seekg(newfilelength+4);
   for(i=0;i<leafnum;i++)
   {
           infile.read((char *)&huffman[i].data,sizeof(unsigned char));
           infile.read((char *)&huffman[i].count,sizeof(unsigned char));
           codelength=(int)huffman[i].count;
           int diff=codelength%8;
           if(0==codelength%8)
               codelength=codelength/8;
          else
               codelength=codelength/8+1;
          huffman[i].code[0]='\0';
          for(int j=0;j<codelength;j++)
          {
                  infile.read((char *)&temp,1);
                  change(temp,code);
                  strcat(huffman[i].code,code);
          }
          int bitlen=strlen(huffman[i].code);
          if(0!=diff)
          huffman[i].code[bitlen-8+diff]='\0';
   }

//编码排序
   for(i=0;i<leafnum;i++)
   {
         for(int j=0;j<leafnum-i-1;j++)
         {
              if(huffman[j].count>huffman[j+1].count)
              {
                  tmp=huffman[j];
                  huffman[j]=huffman[j+1];
                  huffman[j+1]=tmp;
              }
         }
   }
//对文件进行解码
   hasread=0;
   haswrite=0;
   ofstream outfile(outfilename,ios::binary|ios::out);
   if(!outfile)
   {
        return;
   }
   char buf[513]="\0";
   char buf1[257]="\0";
   infile.seekg(108);
   while(1)
   {
        while(hasread<(newfilelength-8)&&strlen(buf)<=256)
        {
              infile.read((char *)&temp,sizeof(temp));
              change(temp,code);
              strcat(buf,code);
              hasread++;
        }
        while(strlen(buf)>=256)
        {
              for(i=0;i<strlen(buf);i++)
              {
                    fuzhi(buf1,buf,i+1);
                    if(pipei(buf1,huffman,leafnum,temp)==1)
                    {
                         outfile.write((char *)&temp,sizeof(unsigned char));
                         haswrite++;
                         strcpy(buf,buf+i+1);
                         break;
                    }
              }
              if(haswrite>=oldfilelength) break;
        }
        if(hasread>=(newfilelength-108)||haswrite>=oldfilelength) break;
   }
   while(haswrite<oldfilelength)
   {
         for(i=0;i<strlen(buf);i++)
         {
              fuzhi(buf1,buf,i+1);
              if(pipei(buf1,huffman,leafnum,temp)==1)
              {
                   outfile.write((char *)&temp,sizeof(unsigned char));
                   haswrite++;
                   strcpy(buf,buf+i+1);
                   break;
             }
         }
    }
    infile.close();
    outfile.close();
}

void change(unsigned char a,char code[])
{
   int n=9;
   for(int i=0;i<n;i++) code[i]='0';
   code[n-1]='\0';
   n=n-2;
   int c=(int)a;
   while(c>0)
   {
         code[n--]=c%2+'0';
         c=c/2;
   }
}

int pipei(char buf[],Node_T head[],int n,unsigned char &c)
{
    for(int i=0;i<n;i++)
        if(strcmp(buf,head[i].code)==0)
        {
               c=head[i].data;
               return 1;
        }
    return 0;
}

void fuzhi(char buf[],char a[],int j)
{
     int i;
     for(i=0;i<j;i++)
        buf[i]=a[i];
     buf[i]='\0';
}
