﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidgetItem>
#include <QFile>
#include <iomanip>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QString loadpath;   //文件的路径
    QString savepath;   //保存路径
    QString news;   //文件详细信息
    QTableWidgetItem *filenames; //显示文件

private slots:

private slots:
    void on_unlockButton_clicked();
    void on_lockButton_clicked();
    void on_loadButton_clicked();
    void on_resetButton_clicked();
};

#endif // MAINWINDOW_H
