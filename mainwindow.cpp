﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "yasuo.h"
#include "jieya.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QFileInfo>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_loadButton_clicked()
{
    loadpath = QFileDialog::getOpenFileName(this, tr("打开"), ".",
                                        tr("*.*"));
    QFile file(loadpath);
    int size = file.size();
    QFileInfo fileinfo(loadpath);
    QString filename = fileinfo.baseName();
    ui->tableWidget->setRowCount(1);
    filenames = new QTableWidgetItem;
    filenames->setText(filename);
    ui->tableWidget->setItem(0, 0, filenames);
    ui->loadEdit->setReadOnly(true);
    ui->loadEdit->setText(loadpath);
    filenames = new QTableWidgetItem;
    filenames->setText(tr("%1 KB").arg(size / 1024.0));
    ui->tableWidget->setItem(0, 1, filenames);

}

void MainWindow::on_lockButton_clicked()     //压缩
{
    if(ui->loadEdit->text().isEmpty())
    {
        QMessageBox::information(this, tr("提示"), tr("文件路径不能为空!!!"));
        return;
    }
    savepath = QFileDialog::getSaveFileName(this, tr("保存"), loadpath, tr("*.huf"));
    yasuo(loadpath.toAscii().constData(), savepath.toAscii().constData());
    QMessageBox::information(this, tr("提示"), tr("压缩成功!!!"));
}

void MainWindow::on_unlockButton_clicked()   //解压
{
    char filename[100];
    ifstream infile;
    if(ui->loadEdit->text().isEmpty())
    {
        QMessageBox::information(this, tr("提示"), tr("文件路径不能为空!!!"));
        return;
    }
    infile.open(loadpath.toAscii().constData(), ios::in|ios::binary);       //打开待压缩的文件
    infile.clear();
    infile.seekg(0);
    infile.read((char *)&filename, 100);
    infile.close();
    QString savepath1(filename);
    savepath = QFileDialog::getSaveFileName(this, tr("保存"), savepath1, tr("*.*"));
    jieya(loadpath.toAscii().constData(), savepath.toAscii().constData());
    QMessageBox::information(this, tr("提示"), tr("解缩成功!!!"));
}

void MainWindow::on_resetButton_clicked()
{
    ui->loadEdit->clear();
}
